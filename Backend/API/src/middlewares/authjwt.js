const config = require('../config');
const jwt = require('jsonwebtoken');
const pool = require('../database');
module.exports = verifyToken = async(req,res,next)=>{
    try{
        const token = req.headers["x-access-token"];
        // console.log(token);
        if(!token) return res.status(403).json({mensaje:"no se proporciona ningún token"});
    
        const decoded = jwt.verify(token,config.SECRET);
    
        // console.log(decoded.id);
        const user = await pool.query('SELECT * FROM USUARIOS WHERE ID_USUARIO = ?',[decoded.id]);
        // console.log(user.length);
        if(user.length == 0) return res.status(404).json({mensaje:"Usuario no encontrado"})
    
        next();
    }catch(error){
        return res.status(401).json({mensaje:"No autorizado"});
    }
}
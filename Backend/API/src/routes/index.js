const express= require('express');
const router= express();
const pool = require('../database');
const helpers = require('../lib/helpers');
const verifyToken = require('../middlewares/authjwt')
router.get('/', async(req,res)=>{
    await pool.query('SELECT * FROM Usuarios',(error,rows,columns)=>{
        if(!error){
            res.json(rows)
        }else{
            console.log(error)
            res.json({mensaje:error.code})
        }
    });
});
router.get('/:id',async(req,res)=>{
    const {id} = req.params;
    await pool.query('SELECT * FROM Usuarios WHERE ID_USUARIO = ?',[id],(error,rows,columns)=>{
        if(!error){
            res.json(rows[0]);
        }else{
            console.log(error)
            res.json({mensaje:error.code})
        }
    });
});
router.put('/updateuser:id',verifyToken, async(req,res)=>{
    const {NOMBRE,APELLIDO_P,APELLIDO_M,usuario} = req.body;
    const {id} = req.params;
    const updateUser={
        NOMBRE,
        APELLIDO_P,
        APELLIDO_M,
        usuario
    };
    await pool.query('UPDATE USUARIOS SET ? WHERE ID_USUARIO=?',[updateUser,id],(error,rows,columns)=>{
        if(!error){
            res.json({mensaje:'Usuario Actualizado'});
        }else{
            console.log(error)
            res.json({mensaje:error.code});
        }
    });
});
router.delete('/deleteuser:id',verifyToken,async(req,res)=>{
    const {id} = req.params;
    await pool.query('DELETE FROM USUARIOS WHERE ID_USUARIO=?',[id],(error,rows,columns)=>{
        if(!error){
            res.json({mensaje:'Usuario eliminado'})
        }else{
            console.log(error)
            res.json({mensaje:error.code}); 
        }
    });
});
module.exports=router;
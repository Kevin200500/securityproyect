const express= require('express');
const router= express();
const pool = require('../database');
const helpers = require('../lib/helpers');
const jwt = require('jsonwebtoken');
const ObtenerID = require('../lib/auth');
const config = require('../config');
router.post('/signup', async(req,res)=>{
    const {MATRICULA,NOMBRE,APELLIDO_P,APELLIDO_M,usuario,PASSWORD} = req.body;
    let id=0;
    const newUser={
        MATRICULA,
        NOMBRE,
        APELLIDO_P,
        APELLIDO_M,
        usuario,
        PASSWORD : await helpers.encryptPassword(PASSWORD)
    };

    let estado = await pool.query('INSERT INTO USUARIOS SET ?',[newUser]);
    // console.log(estado)
    if(estado.affectedRows != 0){
        id = ObtenerID(newUser.MATRICULA);
        console.log(id);
        const token = jwt.sign({id},config.SECRET,{ expiresIn : 86400 }) //expira en 24hrs
        res.json({token});
    }else{
        res.status(500).json({mensaje:"Error en el Servidor"});
    }

});

router.post('/signin',async (req,res)=>{
    let objeto=null;
    const {MATRICULA,PASSWORD} = req.body;
    const userFound = await pool.query(`SELECT * FROM USUARIOS WHERE MATRICULA = ${MATRICULA}`)
    
    if(userFound.length == 0) return res.status(400).json({mensaje: "USUARIO NO ENCONTRADO"})

    objeto= JSON.parse(JSON.stringify(userFound[0]));
    // console.log(objeto.PASSWORD);

    const matchPassword = await helpers.matchPassword(PASSWORD,objeto.PASSWORD);
    
    if(!matchPassword) return res.status(401).json({token: null, mensaje:'Contraseña Invalida'})

    const token = jwt.sign({id: objeto.ID_USUARIO},config.SECRET,{expiresIn: 86400})

    res.json({token});
});

module.exports = router;
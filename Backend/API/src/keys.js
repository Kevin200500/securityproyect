const config=require('./config');
module.exports={
    database:{
        host: config.DB_HOST,
        user: config.DB_USERNAME,
        password: config.DB_PASSWORD,
        database: config.DB_NAME
    }
};
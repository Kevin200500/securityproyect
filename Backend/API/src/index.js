const express = require('express');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors')
// const fs = require('fs');
// const https = require('https');
// const helmet = require('helmet');
//Inicializo Express
const app = express();
//Configuración
app.set('port', process.env.PORT || 4000); //Declaración del Puerto

//Middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.urlencoded({ extended:false}));
app.use(express.json()); // puedo recibir como mandar JSON´s
app.use(passport.initialize());   //debo inicializar passport
app.use(passport.session());
// app.get(helmet());
// para habilitar las peticiones a traves de Angular
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
//variable Global
//routes
app.use(require('./routes/index'));
app.use(require('./routes/authentication'));
// app.get('/', function(req, res){
//     res.json('hola mundo');
// });
//Comienza el Servidor
// https.createServer({
//     key : fs.readFileSync('src/my_cert.key'),
//     cert: fs.readFileSync('src/my_cert.crt')
// },app).listen(app.get('port'),()=>{
//     console.log("El servidor corre en el puerto:",app.get('port'));
// })
app.listen(app.get('port'),()=>{
    console.log("El servidor corre en el puerto:",app.get('port'));
});
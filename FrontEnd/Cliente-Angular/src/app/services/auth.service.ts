import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router'
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  API_URI = 'http://localhost:4000';

  constructor(private http: HttpClient,private router : Router) { }

  signIn(user: any){
    return this.http.post<any>(this.API_URI + '/signin',user);
  }
  public loggedIn(){
    return !! localStorage.getItem('x-access-token');
  }
  getToken(){
    return localStorage.getItem('x-access-token');
  }
  public logout(){
    localStorage.removeItem('x-access-token');
    this.router.navigate(['/signin'])
  }
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {USUARIOS} from '../models/USUARIOS';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  API_URI = 'http://localhost:4000';

  constructor(private http : HttpClient) { }

  getUsers(){
    return this.http.get(`${this.API_URI}/`);
  }

  getUser(id: number){
    return this.http.get<USUARIOS>(`${this.API_URI}/${id}`);
  }

  deleteUser(id: number){
    return this.http.delete(`${this.API_URI}/deleteuser${id}`);
  }
  saveUser(user : USUARIOS){
      return this.http.post(`${this.API_URI}/signup`,user);
  }

  updateUser(id: number|undefined, updateUsuario: USUARIOS) : Observable<USUARIOS>{
    return this.http.put<USUARIOS>(`${this.API_URI}/updateuser${id}`,updateUsuario);
  }
}

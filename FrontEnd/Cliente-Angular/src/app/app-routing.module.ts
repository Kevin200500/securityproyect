import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserFormComponent} from './components/user-form/user-form.component';
import {UserEditComponent} from './components/user-edit/user-edit.component';
import {SigninComponent} from './components/signin/signin.component'
import {AuthGuard} from './auth.guard';
const routes: Routes = [
  {
    path: 'users',
    component: UserListComponent
  },
  {
    path : 'createuser',
    component : UserFormComponent
  },
  {
    path:'updateuser/:id',
    component:UserEditComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signin',
    component: SigninComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, HostBinding, OnInit } from '@angular/core';
import { fromEventPattern } from 'rxjs';
import { USUARIOS } from 'src/app/models/USUARIOS';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router'
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @HostBinding('class') clases = 'row';
  usuarios : any = [];
  constructor(private userService : UsersService, private authService: AuthService,
    private router : Router) { }

  ngOnInit(): void {
    this.getUsers();
  }
  getUsers(){
    this.userService.getUsers().subscribe(
      res => {
        this.usuarios = res;
      },
      err => console.error(err)
    );
  }
  deleteUser(id: number){
    if(this.authService.loggedIn()){
      console.log(id);
      this.userService.deleteUser(id).subscribe(
        res => {
          console.log(res);
          this.getUsers();
        },
        err => console.error(err)
      )
    }else{
      this.router.navigate(['/signin']);
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { USUARIOS } from 'src/app/models/USUARIOS';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  users : USUARIOS = {
    MATRICULA : '',
    PASSWORD : ''
  };
  constructor(private authService: AuthService,private router : Router) { }

  ngOnInit(): void {
  }
  signIn(){
    this.authService.signIn(this.users).subscribe(
      res=>{
        console.log(res);
        localStorage.setItem('x-access-token',res.token)
        this.router.navigate(['/users']);
      },
      err=>{
        console.error(err);
      }
    )
  }
}

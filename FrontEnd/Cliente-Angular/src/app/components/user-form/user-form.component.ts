import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { USUARIOS } from 'src/app/models/USUARIOS';
import {UsersService} from '../../services/users.service';
// impor {} from 
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @HostBinding('class') clases = 'row';
  users : USUARIOS = {
    ID_USUARIO : 0,
    MATRICULA : '',
    NOMBRE : '',
    APELLIDO_P:'',
    APELLIDO_M: '',
    usuario : 1,
    PASSWORD : ''
  };
  constructor(private usersService : UsersService, private router : Router) { }

  ngOnInit(): void {
  }

  signup(){
    this.usersService.saveUser(this.users).subscribe(
      (res:any) => {
        console.log(res);
        localStorage.setItem('x-access-token',res.token)
        this.router.navigate(['/users']);
      },
      err => console.error(err)
    );
  }

}

import { Component, OnInit, HostBinding} from '@angular/core';
import { USUARIOS } from 'src/app/models/USUARIOS';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../services/users.service'
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  @HostBinding('class') clases = 'row';
  users : USUARIOS = {
    ID_USUARIO : 0,
    MATRICULA : '',
    NOMBRE : '',
    APELLIDO_P:'',
    APELLIDO_M: '',
    usuario : 1,
    PASSWORD : ''
  };
  constructor(private usersService : UsersService ,private activatedRoute : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    const params = this.activatedRoute.snapshot.params;
    console.log(params);
    if(params.id){
      this.usersService.getUser(params.id).subscribe(
        res=>{
          console.log(res);
          this.users = res;
        },
        err =>{ console.error(err);}
      );
    }
  }

  update(){
    const id = this.users.ID_USUARIO;
    delete this.users.ID_USUARIO;
    delete this.users.MATRICULA;
    delete this.users.PASSWORD;
    this.usersService.updateUser(id,this.users).subscribe(
      res=>{
        console.log(res);
        this.router.navigate(['/users']);
      },
      err => {
        console.error(err);
      }
    );
  }


}
